#!/usr/bin/python3
"""Redirection to a moved location"""

import os
import sys
from urllib.request import urlretrieve

BASE = "https://git.kernel.org/pub/scm/linux/kernel/git/superm1/amd-debug-tools.git/"

if __name__ == "__main__":
    print(f"This script has been moved to {BASE}")
    download = input("Download updated version (y/N)? ")
    if "y" in download.lower():
        script = os.path.basename(sys.argv[0])
        uri = f"{BASE}/plain/{script}"
        if os.path.exists(script):
            os.remove(script)
        urlretrieve(uri, script)
        print(f"Downloaded {script}")
